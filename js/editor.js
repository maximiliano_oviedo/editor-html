var EDITOR = EDITOR || {};

EDITOR.editor = null; 

function Editor()
{
	this.texto = null;
	this.areaEditor = null;
	this.areaShow = null;
	this.areaHTML = null;
	this.listaTexto = null;

	this.iniciarOmision = false;
	this.terminarOmision = false;

	this.linkClass = "";
	this.linkToGo = "";

	this.etiquetaInicio = [];
	this.etiquetaCierre = [];
};

Editor.prototype.Init = function()
{
	this.initVariables();
	this.initEventListeners();
	this.initFontShowSize();
};

Editor.prototype.initFontShowSize = function()
{
	var show = document.getElementById("show");
	show.style.fontSize = 22 + "px";
	this.setFontSizeToShow(22);
};

Editor.prototype.initVariables = function()
{
	this.areaEditor = document.getElementById("textEditor");
	this.areaHTML = document.getElementById("textHTML");
	this.areaShow = document.getElementById("show");
	this.texto = new Texto("");
};

Editor.prototype.initEventListeners = function()
{
	this.areaEditor.addEventListener('keyup', OnKeyUp);
	this.areaEditor.addEventListener('mouseup', OnMouseUp);
    this.areaEditor.addEventListener("paste", OnPaste);
};

Editor.prototype.actualizarTextosAMostrar = function(event)
{
	var key = event.keyCode || event.which;
	this.texto.textoSeleccion = window.getSelection().toString();
	this.revisarTextoParaActualizar(key);
	this.actualizarTextoAMostrarConEstilos();
};

Editor.prototype.revisarTextoParaActualizar = function(key)
{
	var listaTextoIntroducido = this.pasarTextoACadena(this.getTextoDelAreaEditar());
	this.texto.textoHTML = this.convertirListaATexto(listaTextoIntroducido);
};

Editor.prototype.getTextoDelAreaEditar = function()
{
	return this.areaEditor.innerHTML;
};

Editor.prototype.pasarTextoACadena = function(texto)
{
	var cadena = [];
	for(var i = 0; i < texto.length; ++i)
		cadena.push(texto[i]);
	
	return cadena;
};

Editor.prototype.pasarCadenaATexto = function(lista)
{
	var texto = "";
	for(var i = 0; i < lista.length; ++i)
		texto += lista[i];
	
	return texto;
};

Editor.prototype.hayEtiquetaDeEntradaEnElTexto = function(index, lista)
{
	if(lista[index] == "<")
		this.iniciarOmision = true;
};

Editor.prototype.hayEtiquetaDeSalidaEnElTexto = function(index, lista)
{
	if(lista[index] == ">")
		this.terminarOmision = true;
};

Editor.prototype.convertirListaATexto = function(listaTextoNuevo)
{
	var text = "";
	for(var i = 0; i < listaTextoNuevo.length; ++i)
		text += listaTextoNuevo[i];

	return text; 
};

Editor.prototype.actualizarTextoAMostrarConEstilos = function()
{
	var show = document.getElementById("show");
	this.texto.textoAnterior = this.texto.textoHTML;

	show.innerHTML = this.texto.textoHTML;
};

Editor.prototype.getTextoDelAreaMostrar = function()
{
	var show = document.getElementById("show");
	return show.innerHTML;
};

Editor.prototype.getTextoDelAreaMostrarHTML = function()
{
	var show = document.getElementById("show");
	return show.innerHTML;
};

Editor.prototype.actualizarAreas = function() 
{
	this.areaShow.innerHTML = this.texto.textoLimpio;
};

Editor.prototype.hayTextoSeleccionado = function() 
{
	if(this.texto.textoSeleccion != "")
		return true;

	return false;
};

Editor.prototype.obtenerTextoSinEtiqueta = function(clase)
{
	if(typeof window.getSelection == "undefined") return;
	
	var range = this.obtenerRangoDeLaSeleccionDeTexto();
	if(this.laSeleccionDelTextoNoEsDelCuadroAEditar(range)) return;
	
	var lista = this.obtenerListaDelTextoConFormato();
	var inicioIndex = range.startOffset;
	var indiceInicio = this.obtenerIndiceInicialDelTextoSeleccionadoParaAgregarTag(range, lista, inicioIndex);

	this.agregarTagDeAcuerdoALaClase(indiceInicio, lista, inicioIndex, clase);
};

Editor.prototype.obtenerListaDelTextoConFormato = function()
{
	var texto = this.getTextoDelAreaMostrarHTML();
	var lista = this.pasarTextoACadena(texto);

	return lista;
};

Editor.prototype.obtenerRangoDeLaSeleccionDeTexto = function()
{
	return window.getSelection().getRangeAt(0);
};

Editor.prototype.laSeleccionDelTextoNoEsDelCuadroAEditar = function(range)
{
	var selParent = range.commonAncestorContainer.parentNode;
	if(selParent.id != "textEditor") { console.log("Seleccion no aceptada"); return true; }

	return false;
};

Editor.prototype.obtenerIndiceInicialDelTextoSeleccionadoParaAgregarTag = function(range, lista, inicioIndex)
{
	var indice = 0;

	for(var i = 0; i < lista.length; ++i)
	{		
		if(lista[i] == ">") continue;
		if(lista[i] == "<") { i = this.obtenerIndiceConElSaltoDeEtiqueta(lista, i); continue; }
		if(indice == inicioIndex)	
			return i;

		 ++indice;
	}

	return -1;
};

Editor.prototype.agregarTagDeAcuerdoALaClase = function(index, lista, inicioIndex, clase)
{
	if(clase == "link" || clase == "mail")
		this.agregarLink(lista, index, clase);
	else if(clase == "salto")
		this.agregarSaltoDeLinea(lista, index);
	else
		this.obtenerTextoDeSeleccionDeAcuerdoAlRango(lista, index, inicioIndex, clase);
};

Editor.prototype.obtenerIndiceConElSaltoDeEtiqueta = function(lista, indice)
{
	while(lista[indice] != ">")
		++indice;

	return indice;
};

Editor.prototype.agregarLink = function(lista, indice, clase)
{
	var range = this.obtenerRangoDeLaSeleccionDeTexto();
	
	if(this.isFinalIndexLowerThanStartIndex(range)) return;

	this.elegirEtiquetaDeLinkAUsarse(clase);

	var listaFinal = this.agregarEtiquetaDeInicioALaSeleccion(indice, lista);

	this.texto.textoHTML = this.convertirListaATexto(listaFinal);
	this.actualizarTextoAMostrarConEstilos();
};

Editor.prototype.isFinalIndexLowerThanStartIndex = function(range)
{
	var startIndex = range.startOffset;
	var endIndex = range.endOffset;

	if(startIndex >= endIndex)
	{
		console.log("Error al obtener índices");
		return true;
	}

	return false;
};

Editor.prototype.elegirEtiquetaDeLinkAUsarse = function(clase)
{
	var tagLink = [];
	if(clase == "link")
		tagLink = TAGS.getLinkTagString(this.linkClass, this.linkToGo);
	else
		tagLink = TAGS.getLinkMailTagString(this.linkClass, this.linkToGo);

	this.setEtiquetaAUsar(tagLink[0], tagLink[1]);
};

Editor.prototype.obtenerTextoDeSeleccionDeAcuerdoAlRango = function(lista, indice, inicioIndex, clase)
{
	var range = this.obtenerRangoDeLaSeleccionDeTexto();
	if(this.elIndiceInicialEsMayorOIgualAlIndiceFinal(range)) return;
	if(this.revisarSiHayClaseEnLaSeleccion(indice, lista, clase)) return;
	
	this.setEtiquetaAUsar(TAGS.getInicioSpan(clase), TAGS.getFinalSpan());
	
	var listaFinal = this.agregarEtiquetaDeInicioALaSeleccion(indice, lista);

	this.texto.textoHTML = this.convertirListaATexto(listaFinal);
	this.actualizarTextoAMostrarConEstilos();
};

Editor.prototype.elIndiceInicialEsMayorOIgualAlIndiceFinal = function(range)
{
	var inicioIndex = range.startOffset;
	var finalIndex = range.endOffset;
	if(inicioIndex >= finalIndex) { console.log("Error en la clase"); return true; }
	return false;
};

Editor.prototype.revisarSiHayClaseEnLaSeleccion = function(indice, lista, claseAInsertar)
{
	range = this.obtenerRangoDeLaSeleccionDeTexto();
	var inicioIndex = range.startOffset;
	var indiceARevisar = indice - 1;
	if(indiceARevisar < 0) return false;
	
	var clasesLista = [];
	if(lista[indiceARevisar] == ">")
		clasesLista = this.obtenerListaDeClasesDeLaSeleccionHecha(lista, indiceARevisar, clasesLista);

	return this.laClaseYaHaSidoUsadaEnElTextoSeleccionado(clasesLista, claseAInsertar);
};

Editor.prototype.obtenerListaDeClasesDeLaSeleccionHecha = function(lista, indice, clasesLista)
{
	for(var i = indice; i >= 0; --i)
	{
		if(lista[i] == '"')
		{
			--i;
			clasesLista.push("-");
			while(lista[i] != '"')
			{
				clasesLista.push(lista[i]);
				--i;
			}
		}

		if(lista[i] == "<")
		{
			var nuevoIndice = i - 1;
			if(nuevoIndice < 0) return clasesLista;
			if(lista[nuevoIndice] == ">")
				this.obtenerListaDeClasesDeLaSeleccionHecha(lista, nuevoIndice, clasesLista);

			break;
		}			
	}

	return clasesLista;
};

Editor.prototype.laClaseYaHaSidoUsadaEnElTextoSeleccionado = function(clasesLista, claseAInsertar)
{
	clasesLista.reverse();
	if(clasesLista.length > 0)
	{
		var clase = "";
		for(var i = 0; i < clasesLista.length; ++i)
		{
			if(clasesLista[i] == "-")
			{
				if(claseAInsertar == clase)
				{
					return true;
				}
				clase = "";
				continue;
			}
			clase += clasesLista[i];
		}
	}

	return false;
};

Editor.prototype.setEtiquetaAUsar = function(inicio, cierre)
{
	this.etiquetaInicio = inicio;
	this.etiquetaCierre = cierre;
};

Editor.prototype.agregarEtiquetaDeInicioALaSeleccion = function(indice, lista)
{
	var etiquetaBegin = this.etiquetaInicio;
	var inicioIndex = this.getBeginIndex();
	for(var i = indice, j = 0; i < (indice + etiquetaBegin.length); ++i, ++j)
		lista.splice(i, 0, etiquetaBegin[j]);

	indice += etiquetaBegin.length;

	return this.agregarEtiquetaDeCierreALaSeleccion(indice, lista, inicioIndex);
};

Editor.prototype.getBeginIndex = function()
{
	return this.obtenerRangoDeLaSeleccionDeTexto().startOffset;
};

Editor.prototype.agregarEtiquetaDeCierreALaSeleccion = function(indice, lista, inicioIndex)
{
	var finalIndex = this.getFinalIndex();
	var etiquetaEnd = this.etiquetaCierre;
	var etiquetaEndLength = etiquetaEnd.length;
	var rangoSeleccionado = finalIndex - inicioIndex;
	var indiceFinalRango = indice + rangoSeleccionado;

	for(var i = indiceFinalRango, j = 0; i < (indiceFinalRango + etiquetaEndLength); ++i, ++j)
		lista.splice(i, 0, etiquetaEnd[j]); 

	return lista;
};

Editor.prototype.getFinalIndex = function()
{
	return this.obtenerRangoDeLaSeleccionDeTexto().endOffset;
};

Editor.prototype.agregarSaltoDeLinea = function(lista, indice)
{
	var range = this.obtenerRangoDeLaSeleccionDeTexto();
	if(this.elIndiceInicialEsDiferenteAlIndiceFinal(range)) return;
	
	var listaFinal = this.agregarTagDeSalto(TAGS.getSalto(), lista, indice);

	this.texto.textoHTML = this.convertirListaATexto(listaFinal);
	this.actualizarTextoAMostrarConEstilos();
};

Editor.prototype.elIndiceInicialEsDiferenteAlIndiceFinal = function(range)
{
	var inicioIndex = range.startOffset;
	var finalIndex = range.endOffset;
	if(inicioIndex != finalIndex) { console.log("Error en el salto"); return true; }

	return false;
};

Editor.prototype.agregarTagDeSalto = function(saltoLista, lista, indice)
{
	for(var i = 0; i < saltoLista.length; ++i)
	{
		lista.splice(indice, 0, saltoLista[i]);
		++indice;
	}

	return lista;
};

Editor.prototype.setFontSizeToShow = function(size)
{
	var text = document.getElementById("showSize");
	text.innerHTML = size + "px";
};