//
//
// JS Editor HTML
//
// CMM
//

window.addEventListener('load', Initialize);

function Initialize ()
{
	EDITOR.editor = new Editor();
	EDITOR.editor.Init();

	INPUT.modelo = new Modelo();
	INPUT.modelo.Init();
}

var INPUT = INPUT || {};
INPUT.modelo = null;

function Modelo ()
{
	this.area = null;
	this.element = null;
	this.texto = [];
	this.textoStyle = [];
	this.cursorPos = 0;	
	this.cursorNodeIndex = 0;	
	this.cursorNodeFrame = 0;
	this.numberOfKeysDowns = 0;
	this.isInModifierState = false;
	this.wasLastInputAModifier = false;
	this.accentButtonPressed = false;
	this.isEspecial = false;
	this.isShift = false;	
	this.isCAPS = false;
	this.isTab = false;
	this.isAlt = false;
	this.isEnter = false;

	this.isTeclaFlecha = false;
	this.isIzq = false;
	this.isDer = false;
	this.isArriba = false;
	this.isAbajo = false;

	this.stringHTML = "";
}

Modelo.prototype.Init = function()
{
	this.element = document.getElementById('input');	
	this.initEventListeners();		
}

Modelo.prototype.initEventListeners = function()
{
	var input = document.getElementById('input');
	//input.addEventListener('input', this.OnInput);
	//input.addEventListener('keypress', this.OnKeyPress);
	input.addEventListener('keydown', this.OnKeyDownCallback);
	input.addEventListener('keyup', this.OnKeyUpCallback);
	input.addEventListener('click', this.OnClick);
	//input.addEventListener('select', this.OnSelect);
	input.addEventListener("paste", this.OnPaste);
}

Modelo.prototype.OnInput = function(e)
{	
	//INPUT.modelo.texto.push(e.key);
	console.log(e);	
	//e.preventDefault();
}

Modelo.prototype.OnKeyDownCallback = function(e)
{		
	INPUT.modelo.OnKeyDownLogic(e, e.keyCode, e.key);	
}

Modelo.prototype.OnKeyDownLogic = function(event, keyCode, key)
{											
	if(event.repeat)	
		return this.cancelarInput(event);

	else if(key == 'Backspace' || keyCode == 8)
		return this.borrar(event);

	else if(this.esAlgunaTeclaEspecial(keyCode))
		return this.setTeclaEspecial(event);

	else if(this.seEstaMoviendoConLasFlechas(keyCode))
		return this.moverLaSeleccion(event);

	else if(!this.accentButtonPressed && keyCode == 229)
		return this.verifyAcento(event);

	else if(this.isInModifierState || event.getModifierState(key))
		return this.verifyModifierState(event);
	
	else if(!this.isInModifierState && this.esUnCharacterValido(keyCode))
		return this.procesarInput(key);		

	event.preventDefault();
}

Modelo.prototype.borrar = function(event)
{	
	this.cursorPos -= this.cursorPos > 0 ? 1 : 0;
	this.texto.splice(this.cursorPos, 1);
	this.borrarTextStyle(event);
	this.pintarTexto();	
	return this.cancelarInput(event);
}

Modelo.prototype.borrarTextStyle = function(event)
{
	var count = 0;
	var offset = 0;
	for(var i = 0; i < this.textoStyle.length; ++i)
	{
		var char = this.textoStyle[i];
		var charArray = char.split('');
		if(count === this.cursorPos)
		{			
			this.textoStyle.splice(offset, 1);
			break;
		}
		else if(charArray.length > 1)
		{
			console.log('Char length', charArray);
			if(charArray[0] === '{' && charArray[charArray.length - 1] === '}')
			{
				console.log('char etiqueta');
				++offset;
				continue;
			}
		}	
		++count; 	
		++offset;									
	}
}

Modelo.prototype.verifyAcento = function(event)
{	
	this.accentButtonPressed = true;
	return this.cancelarInput(event);
}

Modelo.prototype.cancelarInput = function(event)
{	
	console.log('cancelar');
	event.preventDefault();	
	return false;
}

Modelo.prototype.teclasEspeciales = {
	'tab': 9, 'alt': 18, 'CAPS': 20, 'shift': 16, 'enter': 13
};

Modelo.prototype.esAlgunaTeclaEspecial = function(keyCode)
{	
	this.isEspecial = false;

	if(keyCode == this.teclasEspeciales.shift && (this.isEspecial = true))
		this.isShift = true;
		
	else if(keyCode == this.teclasEspeciales.CAPS && (this.isEspecial = true))
		this.isCAPS = this.isCAPS ? false : true;
		
	else if(keyCode == this.teclasEspeciales.tab && (this.isEspecial = true))
		this.isTab = true;
		
	else if(keyCode == this.teclasEspeciales.alt && (this.isEspecial = true))
		this.isAlt = true;
		
	else if(keyCode == this.teclasEspeciales.enter && (this.isEspecial = true))
		this.isEnter = true;

	return this.isEspecial;
}


Modelo.prototype.setTeclaEspecial = function(event)
{
	console.log('Is special');
	if(this.isTab)
	{
		this.isTab = false;
		return false;
	}
	else if(this.isCAPS)
	{
		console.log('CAPS on');
		return false;
	}	
	else if(this.isAlt)
	{
		console.log('Alt');
		return false;
	}		
	else if(this.isEnter)
	{
		this.isEnter = false;
		return this.cancelarInput(event);		
	}

	return true;
}

Modelo.prototype.flechas = {
	'arriba': 38, 'abajo': 40, 'izq': 37, 'der': 39
};

Modelo.prototype.seEstaMoviendoConLasFlechas = function(keyCode)
{
	this.isTeclaFlecha = false;

	if(keyCode == this.flechas.izq && (this.isTeclaFlecha = true))
	{
		this.isIzq = true;
	}
	else if(keyCode == this.flechas.der && (this.isTeclaFlecha = true))
	{
		this.isDer = true;
	}
	else if(keyCode == this.flechas.arriba && (this.isTeclaFlecha = true))
	{
		this.isArriba = true;
	}
	else if(keyCode == this.flechas.abajo && (this.isTeclaFlecha = true))
	{
		this.isAbajo = true;
	}

	return this.isTeclaFlecha;
}

Modelo.prototype.moverLaSeleccion = function(event)
{
	if(this.isDer)
	{
		this.isDer = false;
		this.cursorPos += this.cursorPos < this.texto.length ? 1 : 0;		
		this.desplazarCursorEnTexto(this.cursorPos);
	}
	else if(this.isIzq)
	{						
		this.isIzq = 0;
		this.cursorPos -= this.cursorPos > 0 ? 1 : 0
		this.desplazarCursorEnTexto(this.cursorPos);
	}
	else if(this.isAbajo || this.isArriba)
	{
		this.isAbajo = false;
		this.isArriba = false;		
	}

	return this.cancelarInput(event);
}

Modelo.prototype.desplazarCursorEnTexto = function()
{	
	if(this.cursorPos > 0 && this.cursorPos <= this.texto.length)
	{
		var range = document.createRange();
		var sel = window.getSelection();

		console.log(sel);

		var focusNode = sel.focusNode;	
		++this.cursorNodeIndex;

		var nodeActive = sel.focusNode.childNodes[this.cursorNodeFrame];
		console.log(nodeActive);

		range.setStart(nodeActive, this.cursorNodeIndex);
		range.collapse(true);
		sel.removeAllRanges();
		sel.addRange(range);		
		
		/*var nodeType = focusNode.nodeType;
		var parentNode = null;
		if(nodeType == 1)
		{			
			range.setStart(this.element.childNodes[0], posicion);
			range.collapse(true);
			sel.removeAllRanges();
			sel.addRange(range);
			return;
		}
		else
		{
			//parentNode = this.getParentContentEditable(focusNode);
		}*/	
		
		
	}
}

Modelo.prototype.verifyModifierState = function(event)
{	
	if(!this.isMetaKey(event) && !this.isShiftKey(event) && !this.isControlKey(event))
	{		
		this.procesarInput(event.key);
	}	
	
	this.isInModifierState = true;	
	this.isEspecial = false;
	this.wasLastInputAModifier = true;
	return false;
}

Modelo.prototype.isMetaKey = function(event)
{
	return event.metaKey;
}

Modelo.prototype.isShiftKey = function()
{
	return event.shiftKey;
}

Modelo.prototype.isControlKey = function()
{
	return event.ctrlKey;
}

Modelo.prototype.acentosList = {
	'a': 'á', 'A': 'Á',
	'e': 'é', 'E': 'É',
	'i': 'í', 'I': 'Í',
	'o': 'ó', 'O': 'Ó',
	'u': 'ú', 'U': 'Ú'
};

Modelo.prototype.procesarInput = function(key)
{				
	if(this.accentButtonPressed)
	{		
		this.accentButtonPressed = false;
		var keyAccented = this.acentosList[key];
		if(keyAccented != undefined) key = keyAccented;
	}	
	
	this.isEspecial = false;
	this.isInModifierState = false;	
	event.preventDefault();		

	INPUT.modelo.introducirChar(key);
		
	return false;
}

Modelo.prototype.OnKeyPressCallback = function(e)
{	
	INPUT.modelo.OnKeyPressLogic(e);
}

Modelo.prototype.OnKeyPressLogic = function(event)
{
	
}

Modelo.prototype.esUnCharacterValido = function(char)
{			
	if((char != 38 && char != 39 && char != 40 && char != 37) || this.accentButtonPressed)
		return true;

	return false;
}

Modelo.prototype.introducirChar = function(keyString)
{
	//console.log('Key: ' + keyString);
	//console.log('Cursor pos: ' + this.cursorPos + '   -   length: ' + this.texto.length);	
	if(this.cursorPos >= this.texto.length)
	{
		// Agregar al final		
		this.texto.push(keyString);	
		this.introducirCharEnTextoStyleAlInicio(keyString);			
	}
	else
	{
		// Agregar dentro del arreglo
		this.texto.splice(this.cursorPos, 0, keyString);
		this.introducirCharEnTextoStyle(keyString);		
	}		

	this.cursorPos += 1;	
	
	this.pintarTexto();
	this.desplazarCursorEnTexto();
}

Modelo.prototype.introducirCharEnTextoStyleAlInicio = function(keyString)
{
	this.textoStyle.push(keyString);
}

Modelo.prototype.introducirCharEnTextoStyle = function(keyString)
{		
	var count = 0;
	var offset = 0;
	for(var i = 0; i < this.textoStyle.length; ++i)
	{
		var char = this.textoStyle[i];
		var charArray = char.split('');
		if(count === this.cursorPos)
		{			
			this.textoStyle.splice(offset, 0, keyString);
			break;
		}
		else if(charArray.length > 1)
		{
			console.log('Char length', charArray);
			if(charArray[0] === '{' && charArray[charArray.length - 1] === '}')
			{
				console.log('char etiqueta');
				++offset;
				continue;
			}
		}	
		++count; 	
		++offset;									
	}
	
}

Modelo.prototype.pintarTexto = function()
{	
	this.element.innerHTML = "";
	this.stringHTML = "";
	this.textoStyle.forEach(this.PintarChar);
	this.element.innerHTML = this.stringHTML;		
}

Modelo.prototype.PintarChar = function(char)
{
	var charArray = char.split('');
	
	if(charArray.length > 1)
		INPUT.modelo.pintarEtiquetaHTML(charArray);
	else if(char === ' ')
		INPUT.modelo.stringHTML += '&nbsp';	
	else		
		INPUT.modelo.stringHTML += char;
}

Modelo.prototype.etiquetaIdActiva = [];

Modelo.prototype.pintarEtiquetaHTML = function(charArray)
{
	if(charArray[0] === '{' && charArray[charArray.length - 1] === '}')
	{
		if(charArray.length === 8)
		{
			var etiquetaId = charArray[3] + charArray[4] + charArray[5] + charArray[6];
			this.etiquetaIdActiva.push(etiquetaId);	
			var etiquetaHtml = this.etiquetasDict['{t_' + etiquetaId + '}'].etiqueta;
			console.log('<' + etiquetaHtml + '>');			
			INPUT.modelo.stringHTML += '<' + etiquetaHtml  + '>';
		}
		else
		{
			var lastIndex = this.etiquetaIdActiva.length - 1;
			var etiquetaId = this.etiquetaIdActiva[lastIndex];
			console.log('</' + this.etiquetasDict['{t_' + etiquetaId + '}'].etiqueta + '>');			
			INPUT.modelo.stringHTML += '</' + this.etiquetasDict['{t_' + etiquetaId + '}'].etiqueta + '>';
			this.etiquetaIdActiva.splice(lastIndex, 1);
		}
	}

	console.log(this.etiquetaIdActiva);
}

Modelo.prototype.OnClick = function(e)
{	
	INPUT.modelo.obtenerPosicionDelCursor();
}

Modelo.prototype.obtenerPosicionDelCursor = function()
{
	var focusNode = window.getSelection().focusNode;	
	var nodeType = focusNode.nodeType;
	var parentNode = null;
	this.cursorNodeIndex = window.getSelection().getRangeAt(0).startOffset;

	if(nodeType == 1) // Nodo de tipo div padre input
	{
		parentNode = this.getParentContentEditable(focusNode);		
		this.cursorNodeFrame = 0;
		this.cursorPos = window.getSelection().getRangeAt(0).startOffset;		
		return;
	}
	else
	{
		// Pedir siempre el nodo principal donde se esta editando
		parentNode = this.getParentContentEditable(focusNode);
	}

	// Verificar que el padre sea el indicado
	if(focusNode.nodeType != 1)
	{
		var childNodes = parentNode.childNodes;		
		this.cursorNodeFrame = 0;

		// Encontrar el nodo donde está parado el cursor
		for(var i = 0; i < childNodes.length; ++i)
		{
			var child = childNodes[i];			
			if(child.textContent === focusNode.textContent)
			{													
				this.cursorNodeFrame = i;						
				break;
			}			
		}
				
		var textIndex = 0;
		var seEncontro = false;
		for(var i = 0; i < childNodes.length; ++i)
		{
			var child = childNodes[i];
			var textString = child.textContent;
			var textArray = textString.split('');

			for(var j = 0; j < textArray.length; ++j)
			{
				// Revisar el contenido del nodo, para encontrar el cursor								
				if(this.cursorNodeFrame == i)
				{
					var offset = window.getSelection().getRangeAt(0).startOffset;					
					if (j == offset)
					{
						// Se encontro el cursor
						console.log('El indice es: ' + textIndex);
						seEncontro = true;
						this.cursorNodeIndex = offset;					
						break;
					}	
					else if(j == textArray.length - 1)
					{						
						++textIndex;
						console.log('El indice al final es: ', textIndex);
						this.cursorNodeIndex = offset;
						seEncontro = true;
						break;
					}
				}				
				textIndex += 1;				
			}

			if(seEncontro)
				break;
		}

		// Actualizar el cursor posición a la posicion correcta
		INPUT.modelo.cursorPos = textIndex;
	}	
	
	//INPUT.modelo.cursorPos = window.getSelection().getRangeAt(0).startOffset;

	//var parentNode = this.getParentContentEditable(focusNode); 	
	/*console.log(parentNode);
	console.log(window.getSelection());*/	
}

Modelo.prototype.getParentContentEditable = function(elemento)
{
	if(elemento.nodeType === 3)
		return this.getParentContentEditable(elemento.parentNode);

	var id = elemento.getAttribute('id');
	if(id && id == 'input')
		return elemento;

	return this.getParentContentEditable(elemento.parentNode);
}

Modelo.prototype.OnSelect = function(e)
{
	console.log('select');
	//INPUT.modelo.cursorPos = window.getSelection().getRangeAt(0).startOffset;	
}

Modelo.prototype.OnKeyUpCallback = function(e)
{
	INPUT.modelo.OnKeyUpLogic(event);
}

Modelo.prototype.OnKeyUpLogic = function(event)
{	
	var keyString = event.key;					
	if(this.wasLastInputAModifier)
	{
		this.isInModifierState = false;
	}	
	else if(event.keyCode === this.teclasEspeciales.alt)
	{
		this.isAlt = false;
	}
	else if(event.keyCode === this.teclasEspeciales.CAPS)
	{		
		this.isCAPS = false;
	}
	else if(event.keyCode === this.teclasEspeciales.shift)
	{
		console.log('shift off');
		this.isShift = false;
	}
}

Modelo.prototype.OnPaste = function(e)
{		
	e.preventDefault();

    var text = e.clipboardData.getData("text/plain");
    INPUT.modelo.pegarTextoEnArregloDeTexto(text);
    //document.execCommand("insertHTML", false, text);		
}

Modelo.prototype.pegarTextoEnArregloDeTexto = function(texto)
{
	var textArray = texto.split('');
	textArray.forEach(this.AgregarLetraAlArreglo);	
}

Modelo.prototype.AgregarLetraAlArreglo = function(letra, array)
{
	INPUT.modelo.introducirChar(letra);
}

Modelo.prototype.etiquetasDict = {
	"{t_0000}": {'etiqueta': 'b', 'id': "0000"}
};

Modelo.prototype.etiquetasId = {
	"oscura": "0000"
}

Modelo.prototype.agregarEtiqueta = function(etiqueta)
{
	if(etiqueta == 'oscura')
	{
		var range = document.createRange();
		var sel = window.getSelection();		
		
		var selectionOffset = sel.anchorOffset;
		var selectionEndOffset = sel.focusOffset;
		var selectionLength = selectionEndOffset - selectionOffset;	

		this.textoStyle.splice(selectionOffset, 0, this.getEtiquetaIdAbertura('oscura'));	
		this.textoStyle.splice(selectionOffset + selectionLength + 1, 0, this.getEtiquetaCerrar());	

		range.setStart(this.element.childNodes[0], selectionEndOffset);		

		//sel.removeAllRanges();
		sel.addRange(range);		
	}	

	this.pintarTexto();
	this.obtenerPosicionDelCursor();
}

Modelo.prototype.getEtiquetaIdAbertura = function(nombre)
{
	return "{t_" + this.etiquetasId[nombre] + "}";
}

Modelo.prototype.getEtiquetaCerrar = function(nombre)
{
	return "{t_}";
}

var EDITOR = EDITOR || {};

EDITOR.editor = null; 

function Editor()
{
	this.texto = null;
	this.areaEditor = null;
	this.areaShow = null;
	this.areaHTML = null;
	this.listaTexto = null;

	this.iniciarOmision = false;
	this.terminarOmision = false;

	this.linkClass = "";
	this.linkToGo = "";

	this.etiquetaInicio = [];
	this.etiquetaCierre = [];
};

Editor.prototype.Init = function()
{
	this.initVariables();
	this.initEventListeners();
	this.initFontShowSize();
};

Editor.prototype.initFontShowSize = function()
{
	var show = document.getElementById("show");
	show.style.fontSize = 22 + "px";
	this.setFontSizeToShow(22);
};

Editor.prototype.initVariables = function()
{
	this.areaEditor = document.getElementById("textEditor");
	this.areaHTML = document.getElementById("textHTML");
	this.areaShow = document.getElementById("show");
	this.texto = new Texto("");
};

Editor.prototype.initEventListeners = function()
{
	this.areaEditor.addEventListener('keyup', OnKeyUp);
	this.areaEditor.addEventListener('mouseup', OnMouseUp);
    this.areaEditor.addEventListener("paste", OnPaste);
};

Editor.prototype.actualizarTextosAMostrar = function(event)
{
	var key = event.keyCode || event.which;	
	this.texto.textoSeleccion = window.getSelection().toString();
	this.revisarTextoParaActualizar(key);
	this.actualizarTextoAMostrarConEstilos();
};

Editor.prototype.revisarTextoParaActualizar = function(key)
{
	var listaTextoIntroducido = this.pasarTextoACadena(this.getTextoDelAreaEditar());
	this.texto.textoHTML = this.convertirListaATexto(listaTextoIntroducido);
};

Editor.prototype.getTextoDelAreaEditar = function()
{
	return this.areaEditor.innerHTML;
};

Editor.prototype.pasarTextoACadena = function(texto)
{
	var cadena = [];
	for(var i = 0; i < texto.length; ++i)
		cadena.push(texto[i]);
	
	console.log(cadena);
	return cadena;
};

Editor.prototype.pasarCadenaATexto = function(lista)
{
	var texto = "";
	for(var i = 0; i < lista.length; ++i)
		texto += lista[i];
	
	return texto;
};

Editor.prototype.hayEtiquetaDeEntradaEnElTexto = function(index, lista)
{
	if(lista[index] == "<")
		this.iniciarOmision = true;
};

Editor.prototype.hayEtiquetaDeSalidaEnElTexto = function(index, lista)
{
	if(lista[index] == ">")
		this.terminarOmision = true;
};

Editor.prototype.convertirListaATexto = function(listaTextoNuevo)
{
	var text = "";
	for(var i = 0; i < listaTextoNuevo.length; ++i)
		text += listaTextoNuevo[i];

	return text; 
};

Editor.prototype.actualizarTextoAMostrarConEstilos = function()
{
	var show = document.getElementById("show");
	this.texto.textoAnterior = this.texto.textoHTML;

	show.innerHTML = this.texto.textoHTML;
};

Editor.prototype.getTextoDelAreaMostrar = function()
{
	var show = document.getElementById("show");
	return show.innerHTML;
};

Editor.prototype.getTextoDelAreaMostrarHTML = function()
{
	var show = document.getElementById("show");
	return show.innerHTML;
};

Editor.prototype.actualizarAreas = function() 
{
	this.areaShow.innerHTML = this.texto.textoLimpio;
};

Editor.prototype.hayTextoSeleccionado = function() 
{
	if(this.texto.textoSeleccion != "")
		return true;

	return false;
};

Editor.prototype.obtenerTextoSinEtiqueta = function(clase)
{
	if(typeof window.getSelection == "undefined") return;
	
	var range = this.obtenerRangoDeLaSeleccionDeTexto();
	if(this.laSeleccionDelTextoNoEsDelCuadroAEditar(range)) return;
	
	var lista = this.obtenerListaDelTextoConFormato();
	var inicioIndex = range.startOffset;
	var indiceInicio = this.obtenerIndiceInicialDelTextoSeleccionadoParaAgregarTag(range, lista, inicioIndex);

	this.agregarTagDeAcuerdoALaClase(indiceInicio, lista, inicioIndex, clase);
};

Editor.prototype.obtenerListaDelTextoConFormato = function()
{
	var texto = this.getTextoDelAreaMostrarHTML();
	var lista = this.pasarTextoACadena(texto);

	return lista;
};

Editor.prototype.obtenerRangoDeLaSeleccionDeTexto = function()
{
	return window.getSelection().getRangeAt(0);
};

Editor.prototype.laSeleccionDelTextoNoEsDelCuadroAEditar = function(range)
{
	var selParent = range.commonAncestorContainer.parentNode;
	if(selParent.id != "textEditor") { console.log("Seleccion no aceptada"); return true; }

	return false;
};

Editor.prototype.obtenerIndiceInicialDelTextoSeleccionadoParaAgregarTag = function(range, lista, inicioIndex)
{
	var indice = 0;

	for(var i = 0; i < lista.length; ++i)
	{		
		if(lista[i] == ">") continue;
		if(lista[i] == "<") { i = this.obtenerIndiceConElSaltoDeEtiqueta(lista, i); continue; }
		if(indice == inicioIndex)	
			return i;

		 ++indice;
	}

	return -1;
};

Editor.prototype.agregarTagDeAcuerdoALaClase = function(index, lista, inicioIndex, clase)
{
	if(clase == "link" || clase == "mail")
		this.agregarLink(lista, index, clase);
	else if(clase == "salto")
		this.agregarSaltoDeLinea(lista, index);
	else
		this.obtenerTextoDeSeleccionDeAcuerdoAlRango(lista, index, inicioIndex, clase);
};

Editor.prototype.obtenerIndiceConElSaltoDeEtiqueta = function(lista, indice)
{
	while(lista[indice] != ">")
		++indice;

	return indice;
};

Editor.prototype.agregarLink = function(lista, indice, clase)
{
	var range = this.obtenerRangoDeLaSeleccionDeTexto();
	
	if(this.isFinalIndexLowerThanStartIndex(range)) return;

	this.elegirEtiquetaDeLinkAUsarse(clase);

	var listaFinal = this.agregarEtiquetaDeInicioALaSeleccion(indice, lista);

	this.texto.textoHTML = this.convertirListaATexto(listaFinal);
	this.actualizarTextoAMostrarConEstilos();
};

Editor.prototype.isFinalIndexLowerThanStartIndex = function(range)
{
	var startIndex = range.startOffset;
	var endIndex = range.endOffset;

	if(startIndex >= endIndex)
	{
		console.log("Error al obtener índices");
		return true;
	}

	return false;
};

Editor.prototype.elegirEtiquetaDeLinkAUsarse = function(clase)
{
	var tagLink = [];
	if(clase == "link")
		tagLink = TAGS.getLinkTagString(this.linkClass, this.linkToGo);
	else
		tagLink = TAGS.getLinkMailTagString(this.linkClass, this.linkToGo);

	this.setEtiquetaAUsar(tagLink[0], tagLink[1]);
};

Editor.prototype.obtenerTextoDeSeleccionDeAcuerdoAlRango = function(lista, indice, inicioIndex, clase)
{
	var range = this.obtenerRangoDeLaSeleccionDeTexto();
	if(this.elIndiceInicialEsMayorOIgualAlIndiceFinal(range)) return;
	if(this.revisarSiHayClaseEnLaSeleccion(indice, lista, clase)) return;
	
	this.setEtiquetaAUsar(TAGS.getInicioSpan(clase), TAGS.getFinalSpan());
	
	var listaFinal = this.agregarEtiquetaDeInicioALaSeleccion(indice, lista);

	this.texto.textoHTML = this.convertirListaATexto(listaFinal);
	this.actualizarTextoAMostrarConEstilos();
};

Editor.prototype.elIndiceInicialEsMayorOIgualAlIndiceFinal = function(range)
{
	var inicioIndex = range.startOffset;
	var finalIndex = range.endOffset;
	if(inicioIndex >= finalIndex) { console.log("Error en la clase"); return true; }
	return false;
};

Editor.prototype.revisarSiHayClaseEnLaSeleccion = function(indice, lista, claseAInsertar)
{
	range = this.obtenerRangoDeLaSeleccionDeTexto();
	var inicioIndex = range.startOffset;
	var indiceARevisar = indice - 1;
	if(indiceARevisar < 0) return false;
	
	var clasesLista = [];
	if(lista[indiceARevisar] == ">")
		clasesLista = this.obtenerListaDeClasesDeLaSeleccionHecha(lista, indiceARevisar, clasesLista);

	return this.laClaseYaHaSidoUsadaEnElTextoSeleccionado(clasesLista, claseAInsertar);
};

Editor.prototype.obtenerListaDeClasesDeLaSeleccionHecha = function(lista, indice, clasesLista)
{
	for(var i = indice; i >= 0; --i)
	{
		if(lista[i] == '"')
		{
			--i;
			clasesLista.push("-");
			while(lista[i] != '"')
			{
				clasesLista.push(lista[i]);
				--i;
			}
		}

		if(lista[i] == "<")
		{
			var nuevoIndice = i - 1;
			if(nuevoIndice < 0) return clasesLista;
			if(lista[nuevoIndice] == ">")
				this.obtenerListaDeClasesDeLaSeleccionHecha(lista, nuevoIndice, clasesLista);

			break;
		}			
	}

	return clasesLista;
};

Editor.prototype.laClaseYaHaSidoUsadaEnElTextoSeleccionado = function(clasesLista, claseAInsertar)
{
	clasesLista.reverse();
	if(clasesLista.length > 0)
	{
		var clase = "";
		for(var i = 0; i < clasesLista.length; ++i)
		{
			if(clasesLista[i] == "-")
			{
				if(claseAInsertar == clase)
				{
					return true;
				}
				clase = "";
				continue;
			}
			clase += clasesLista[i];
		}
	}

	return false;
};

Editor.prototype.setEtiquetaAUsar = function(inicio, cierre)
{
	this.etiquetaInicio = inicio;
	this.etiquetaCierre = cierre;
};

Editor.prototype.agregarEtiquetaDeInicioALaSeleccion = function(indice, lista)
{
	var etiquetaBegin = this.etiquetaInicio;
	var inicioIndex = this.getBeginIndex();
	for(var i = indice, j = 0; i < (indice + etiquetaBegin.length); ++i, ++j)
		lista.splice(i, 0, etiquetaBegin[j]);

	indice += etiquetaBegin.length;

	return this.agregarEtiquetaDeCierreALaSeleccion(indice, lista, inicioIndex);
};

Editor.prototype.getBeginIndex = function()
{
	return this.obtenerRangoDeLaSeleccionDeTexto().startOffset;
};

Editor.prototype.agregarEtiquetaDeCierreALaSeleccion = function(indice, lista, inicioIndex)
{
	var finalIndex = this.getFinalIndex();
	var etiquetaEnd = this.etiquetaCierre;
	var etiquetaEndLength = etiquetaEnd.length;
	var rangoSeleccionado = finalIndex - inicioIndex;
	var indiceFinalRango = indice + rangoSeleccionado;

	for(var i = indiceFinalRango, j = 0; i < (indiceFinalRango + etiquetaEndLength); ++i, ++j)
		lista.splice(i, 0, etiquetaEnd[j]); 

	return lista;
};

Editor.prototype.getFinalIndex = function()
{
	return this.obtenerRangoDeLaSeleccionDeTexto().endOffset;
};

Editor.prototype.agregarSaltoDeLinea = function(lista, indice)
{
	var range = this.obtenerRangoDeLaSeleccionDeTexto();
	if(this.elIndiceInicialEsDiferenteAlIndiceFinal(range)) return;
	
	var listaFinal = this.agregarTagDeSalto(TAGS.getSalto(), lista, indice);

	this.texto.textoHTML = this.convertirListaATexto(listaFinal);
	this.actualizarTextoAMostrarConEstilos();
};

Editor.prototype.elIndiceInicialEsDiferenteAlIndiceFinal = function(range)
{
	var inicioIndex = range.startOffset;
	var finalIndex = range.endOffset;
	if(inicioIndex != finalIndex) { console.log("Error en el salto"); return true; }

	return false;
};

Editor.prototype.agregarTagDeSalto = function(saltoLista, lista, indice)
{
	for(var i = 0; i < saltoLista.length; ++i)
	{
		lista.splice(indice, 0, saltoLista[i]);
		++indice;
	}

	return lista;
};

Editor.prototype.setFontSizeToShow = function(size)
{
	var text = document.getElementById("showSize");
	text.innerHTML = size + "px";
};

var TAGS = TAGS || {}; 

TAGS.BoldClick = function()
{
	INPUT.modelo.agregarEtiqueta('oscura');
	//EDITOR.editor.obtenerTextoSinEtiqueta("oscura");
};

TAGS.ItalicClick = function()
{
	EDITOR.editor.obtenerTextoSinEtiqueta("cursiva");		
};

TAGS.SmallClick = function()
{
	EDITOR.editor.obtenerTextoSinEtiqueta("versalita");
};

TAGS.MarkClick = function()
{
	EDITOR.editor.obtenerTextoSinEtiqueta("marka");
};

TAGS.DeletedClick = function()
{
	EDITOR.editor.obtenerTextoSinEtiqueta("del");
};

TAGS.UnderlineClick = function()
{
	EDITOR.editor.obtenerTextoSinEtiqueta("unde");
};

TAGS.OverlineClick = function()
{
	EDITOR.editor.obtenerTextoSinEtiqueta("ove");
};

TAGS.SubscriptClick = function()
{
	EDITOR.editor.obtenerTextoSinEtiqueta("sub");
};

TAGS.SuperscriptClick = function()
{
	EDITOR.editor.obtenerTextoSinEtiqueta("sup");
};

TAGS.BreakLineClick = function()
{
	EDITOR.editor.obtenerTextoSinEtiqueta("salto");
};

TAGS.LinkClick = function()
{
	EDITOR.editor.linkToGo = prompt("Insertar link", null);
	EDITOR.editor.linkClass = prompt("¿Agregar clase al link?", "");
	EDITOR.editor.obtenerTextoSinEtiqueta("link");
};

TAGS.MailClick = function()
{
	EDITOR.editor.linkToGo = prompt("Insertar link", null);
	EDITOR.editor.linkClass = prompt("¿Agregar clase al link?", "");
	EDITOR.editor.obtenerTextoSinEtiqueta("mail");
};

TAGS.aumentarSize = function()
{
	var text = document.getElementById("show");
	
	var textSize = TAGS.getFontStyleSize(text);
	text.style.fontSize = (textSize + 1) + "px";
	EDITOR.editor.setFontSizeToShow(textSize + 1);
};

TAGS.disminuirSize = function()
{
	var text = document.getElementById("show");

	var textSize = TAGS.getFontStyleSize(text);
	text.style.fontSize = (textSize - 1) + "px";
	EDITOR.editor.setFontSizeToShow(textSize - 1);
};

TAGS.getInicioSpan = function(clase)
{
	var spanInicio = "<span class='" + clase + "'>";
	var etiquetaBegin = EDITOR.editor.pasarTextoACadena(spanInicio);
	return etiquetaBegin;
};

TAGS.getFinalSpan = function()
{
	var spanEnd = "</span>";
	return EDITOR.editor.pasarTextoACadena(spanEnd);;
};

TAGS.getSalto = function()
{
	var salto = "<br>";
	return EDITOR.editor.pasarTextoACadena(salto);
};

TAGS.getLinkTagString = function(clase, linkToGo)
{
	var openTag = ""; 

	if(clase != "")
		openTag = '<a href="' + linkToGo + '" target="_blank" class="' + clase + '">';
	else
		openTag = '<a href="' + linkToGo + '" target="_blank">';

	var link = [openTag, '</a>'];	

	return link;
};

TAGS.getLinkMailTagString = function(clase, linkToGo)
{
	var openTag = ""; 

	if(clase != "")
		openTag = '<a href="mailto:' + linkToGo + '" target="_top" class="' + clase + '">';
	else
		openTag = '<a href="mailto:' + linkToGo + '" target="_top">';

	var link = [openTag, '</a>'];	

	return link;
};

TAGS.getFontStyleSize = function(element)
{
	var cs = window.getComputedStyle(element, null);
	var pixelSize = cs.getPropertyValue('font-size');
	var floatSize = parseFloat(pixelSize);
	return floatSize;
}

TAGS.searchTextToReplace = function(htmlText, offsetBegin, offsetEnd) 
{
	var html = "";
	
	if(typeof window.getSelection != "undefined")
	{
		var sel = window.getSelection();
		var text = EDITOR.editor.getTextoDelAreaMostrar();
		var lista = [];
		
		for(var i = 0; i < txt.length; ++i)
		{
			lista.push(txt[i]);
		}
	}
};

TAGS.getSelectedText = function()
{
	var html = "";
	if(typeof window.getSelection != "undefined")
	{
		var sel = window.getSelection();
		if(sel.rangeCount) 
		{
			var container = document.createElement("div");
			for (var i = 0, len = sel.rangeCount; i < len; ++i) 
			{
                container.appendChild(sel.getRangeAt(i).cloneContents());
            }
            html = container.innerHTML;
		} 
		else if (typeof document.selection != "undefined") 
		{
			console.log("createRange");
        	if (document.selection.type == "Text") 
        	{
            	html = document.selection.createRange().htmlText;
        	}
        }
	}	
	return html;
};

TAGS.getHTMLText = function() 
{
	var texto = EDITOR.editor.areaShow.innerHTML; 
	var div = document.getElementById("code");
	code.innerText = texto;
};

function Texto()
{
	this.textoLimpio = "";
	this.textoHTML = "";
	this.textoSeleccion = "";

	this.textoAnterior = "";
};

Texto.prototype.agregarTextoNuevo = function(texto)
{
	var textoAgregar = "";
	
	for(var i = this.textoLimpio.length; i < texto.length; ++i)	
		textoAgregar += texto[i];
	
	this.textoLimpio += textoAgregar;
};

Texto.prototype.setTextoLimpio = function(text)
{
	this.textoLimpio = text;
};

Texto.prototype.setTextoHTML = function(text)
{
	this.textoHTML = text;
};

Texto.prototype.setTextoSeleccionado = function(text)
{
	this.textoSeleccion = text;
};

function OnKeyUp(event)
{
	EDITOR.editor.actualizarTextosAMostrar(event);
}

function OnMouseUp(event)
{ }

function OnPaste(e)
{	
    // cancel paste
    e.preventDefault();

    // get text representation of clipboard
    var text = e.clipboardData.getData("text/plain");

    // insert text manually
    document.execCommand("insertHTML", false, text);
}

function pair(uno, dos)
{
	this.uno = uno;
	this.dos = dos;
}


